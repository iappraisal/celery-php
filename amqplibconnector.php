<?php
/**
 * This file contains a PHP client to Celery distributed task queue
 *
 * LICENSE: 2-clause BSD
 *
 * Copyright (c) 2014, GDR!
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 * @link http://massivescale.net/
 * @link http://gdr.geekhood.net/
 * @link https://github.com/gjedeer/celery-php
 *
 * @package celery-php
 * @license http://opensource.org/licenses/bsd-license.php 2-clause BSD
 * @author  GDR! <gdr@go2.pl>
 */

require_once 'amqp.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Driver for pure PHP implementation of AMQP protocol
 *
 * @link    https://github.com/videlalvaro/php-amqplib
 * @package celery-php
 */
class AMQPLibConnector extends AbstractAMQPConnector
{
    /**
     * How long (in seconds) to wait for a message from queue
     * Sadly, this can't be set to zero to achieve complete asynchronity
     */
    public $wait_timeout = 0.1;

    /**
     * PhpAmqpLib\Message\AMQPMessage object received from the queue
     */
    private $message = null;

    /**
     * AMQPChannel object cached for subsequent getMessageBody() calls
     */
    private $receiving_channel = null;

    public function getConnectionObject($details)
    {
        return new AMQPStreamConnection(
            $details['host'],
            $details['port'],
            $details['login'],
            $details['password'],
            $details['vhost']
        );
    }

    /* NO-OP: not required in PhpAmqpLib */
    public function connect($connection)
    {
    }

    public function postToExchange($connection, $details, $task, $params, $opts = [])
    {
        $ch = $connection->channel();

        $this->declareExchangeQueue($ch, $details, 0, $opts);

        $msg = new AMQPMessage(
            $task,
            $params
        );

        $ch->basic_publish($msg, $details['exchange'], $details['routing_key']);

        $ch->close();

        /* Satisfy Celery::postTask() error checking */
        /* TODO: catch some exceptions? Which ones? */
        return true;
    }

    /**
     * A callback function for AMQPChannel::basic_consume
     *
     * @param PhpAmqpLib\Message\AMQPMessage $msg
     */
    public function consume($msg)
    {
        $this->message = $msg;
    }

    /**
     * Return result of task execution for $task_id
     *
     * @param  object  $connection             AMQPConnection object
     * @param  array   $details                Celery task details
     * @param  int     $expire                 expire time result queue, milliseconds
     * @param  boolean $removeMessageFromQueue whether to remove message from queue
     * @param  array   $opts                   options for declare queue and exchange
     * @return array array('body' => JSON-encoded message body, 'complete_result' => AMQPMessage object)
     *          or false if result not ready yet
     */
    public function getMessageBody($connection, $details, $expire = 0, $removeMessageFromQueue = true, $opts = [])
    {
        static $queues = [];

        if (!$this->receiving_channel) {
            $this->receiving_channel = $connection->channel();
        }

        if (!$this->getQueue($queues, $details['binding'])) {
            $this->declareExchangeQueue($this->receiving_channel, $details, $expire, $opts);
            $queues[$details['binding']] = $details['binding'];
        }

        $message = $this->receiving_channel->basic_get($details['binding']);

        if (!$message) {
            return false;
        }

        return array(
            'message' => $message,
            'body' => $message->body, // JSON message body
            'queue' => $this->receiving_channel
        );
    }

    /**
     * Delete message from queue
     *
     * @param array $message result of task execution (getMessageBody)
     */
    public function deleteMessage(array $message)
    {
        $message['queue']->basic_ack($message['message']->delivery_info['delivery_tag']);
    }

    /**
     * Put message back to the queue
     *
     * @param array $message result of task execution (getMessageBody)
     */
    public function rejectMessage(array $message)
    {
        $message['queue']->basic_nack(
            $message['message']->delivery_info['delivery_tag'],
            $multiple = false,
            $requeue = true
        );
    }

    /**
     * Declare exchange, queue and binding
     *
     * @param AMQPChannel $ch      amqp connection channel object
     * @param array       $details Celery task details
     * @param int         $expire  expire time result queue, milliseconds
     * @param array       $opts    options for declare queue and exchange
     */
    public function declareExchangeQueue($ch, $details, $expire = 0, $opts = [])
    {
        $expire_args = null;

        if (!empty($expire)) {
            $expire_args = array("x-expires"=>array("I",$expire));
        }

        $ch->queue_declare(
            $details['binding'],    /* queue name - "celery" */
            false,                  /* passive */
            $opts['DURABLE'],       /* durable */
            false,                  /* exclusive */
            false,                  /* auto_delete */
            $expire_args
        );

        $ch->exchange_declare(
            $details['exchange'],   /* name */
            'topic',                /* type */
            false,                  /* passive */
            $opts['DURABLE'],       /* durable */
            false                   /* auto_delete */
        );

        $ch->queue_bind(
            $details['binding'],    /* queue name - "celery" */
            $details['exchange'],   /* exchange name - "celery" */
            $details['routing_key']
        );
    }

    /**
     * Pool of queues. Return queue if it exists
     *
     * @param  array  $queues
     * @param  string $binding name of queue
     * @return bool
     */
    private function getQueue(array $queues, $binding)
    {
        return isset($queues[$binding]) ? $queues[$binding] : false;
    }
}
