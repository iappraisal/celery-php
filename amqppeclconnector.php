<?php

require_once 'amqp.php';

/**
 * Driver for a fast C/librabbitmq implementation of AMQP from PECL
 *
 * @link    http://pecl.php.net/package/amqp
 * @package celery-php
 */
class PECLAMQPConnector extends AbstractAMQPConnector
{
    /**
     * @var AMQPQueue current queue
     */
    private $q;

    /**
     * @var AMQPExchange current exchange
     */
    private $xchg;

    /**
     * Return AMQPConnection object passed to all other calls
     *
     * @param  array $details array of connection details
     * @return AMQPConnection
     */
    public function getConnectionObject($details)
    {
        static $connection;

        if (isset($connection)) {
            return $connection;
        }

        // $params = ['heartbeat' => 120];

        // if (isset($details['heartbeat'])) {
        //     $params['heartbeat'] = intval($details['heartbeat']);
        // }

        $connection = new AMQPConnection();
        $connection->setHost($details['host']);
        $connection->setLogin($details['login']);
        $connection->setPassword($details['password']);
        $connection->setVhost($details['vhost']);
        $connection->setPort($details['port']);

        return $connection;
    }

    /**
     * Initialize connection on a given connection object
     *
     * @return NULL
     */
    public function connect($connection)
    {
        static $connected;

        if (isset($connected)) {
            return;
        }

        $connection->connect();
        $connection->channel = new AMQPChannel($connection);

        $connected = true;
    }

    /**
     * Post a task to exchange specified in $details
     *
     * @param  AMQPConnection $connection Connection object
     * @param  array          $details    array of connection details
     * @param  string         $task       JSON-encoded task
     * @param  array          $params     AMQP message parameters
     * @param  array          $opts
     * @return mixed
     */
    public function postToExchange($connection, $details, $task, $params, $opts = [])
    {
        $this->declareExchangeQueue($connection, $details, 0, $opts);

        $success = $this->xchg->publish($task, $details['routing_key'], 0, $params);

        return $success;
    }

    public function processMessage(AMQPEnvelope $message)
    {
        if ($message->getContentType() != 'application/json') {
            throw new CeleryException(
                'Response was not encoded using JSON - found ' .
                $message->getContentType().
                ' - check your CELERY_RESULT_SERIALIZER setting!'
            );
        }

        return array(
            'message' => $message,
            'body' => $message->getBody(),
            'queue' => $q
        );

        return false;
    }

    /**
     * Return result of task execution for $task_id
     *
     * @param  AMQPConnection $connection             Connection object
     * @param  string         $details                array of connection details
     * @param  boolean        $removeMessageFromQueue whether to remove message from queue
     * @param  array          $opts
     * @return array array('body' => JSON-encoded message body, 'complete_result' => AMQPEnvelope object)
     *          or false if result not ready yet
     * @throws CeleryException
     */
    public function consume($connection, $details, $expire = 0, $removeMessageFromQueue = true, $opts = [], callable $callback)
    {
        static $queues = [];

        $q = $this->getQueue($queues, $details['binding']);

        if (!$q) {
            $this->declareExchangeQueue($connection, $details, $expire, $opts);
            $q = $queues[$this->q->getName()] = $this->q;
        }

        $connection->channel->setPrefetchCount($details['prefetch_count']);

        $message = $q->consume(function(AMQPEnvelope $message) use ($callback, $q) {
            if ($message->getContentType() != 'application/json') {
                throw new CeleryException(
                    'Response was not encoded using JSON - found ' .
                    $message->getContentType().
                    ' - check your CELERY_RESULT_SERIALIZER setting!'
                );
            }

            $callback([
                'message' => $message,
                'body' => $message->getBody(),
                'queue' => $q
            ]);
        });
    }

    /**
     * Return result of task execution for $task_id
     *
     * @param  AMQPConnection $connection             Connection object
     * @param  string         $details                array of connection details
     * @param  boolean        $removeMessageFromQueue whether to remove message from queue
     * @param  array          $opts
     * @return array array('body' => JSON-encoded message body, 'complete_result' => AMQPEnvelope object)
     *          or false if result not ready yet
     * @throws CeleryException
     */
    public function getMessageBody($connection, $details, $expire = 0, $removeMessageFromQueue = true, $opts = [])
    {
        static $queues = [];

        $q = $this->getQueue($queues, $details['binding']);

        if (!$q) {
            $this->declareExchangeQueue($connection, $details, $expire, $opts);
            $q = $queues[$this->q->getName()] = $this->q;
        }

        if (!isset($opts['NO_DISCONNECT'])) {
            $connection->channel->setPrefetchCount($details['prefetch_count']);
        }

        $message = $q->get();

        if (!$message) {
            return false;
        }

        if ($message->getContentType() != 'application/json') {
            throw new CeleryException(
                'Response was not encoded using JSON - found ' .
                $message->getContentType().
                ' - check your CELERY_RESULT_SERIALIZER setting!'
            );
        }

        if ($removeMessageFromQueue) {
            $q->delete();
        }

        if (!isset($opts['NO_DISCONNECT'])) {
            $connection->disconnect();
        }

        return array(
            'message' => $message,
            'body' => $message->getBody(),
            'queue' => $q
        );
    }

    /**
     * Delete message from queue
     *
     * @param array $message result of task execution (getMessageBody)
     */
    public function deleteMessage(array $message)
    {
        return $message['queue']->ack($message['message']->getDeliveryTag());
    }

    /**
     * Put message back to the queue
     *
     * @param array $message result of task execution (getMessageBody)
     */
    public function rejectMessage(array $message)
    {
        return $message['queue']->reject(
            $message['message']->getDeliveryTag(),
            AMQP_REQUEUE
        );
    }

    /**
     * Declare exchange, queue and binding
     *
     * @param  AMQPConnection $connection amqp connection object
     * @param  array          $details    Celery task details
     * @param  int            $expire     expire time result queue, milliseconds
     * @param  array          $opts       options for declare queue and exchange
     * @return bool
     */
    public function declareExchangeQueue($connection, $details, $expire = 0, $opts = [])
    {
        static $exchanges, $queues;

        $ch = $connection->channel;

        if (!isset($exchanges)) {
            $exchanges = [];
        }

        if (!isset($queues)) {
            $queues = [];
        }

        if (!empty($exchanges[$details['exchange']])) {
            $this->xchg = $exchanges[$details['exchange']];
        } else {
            $xchg = new AMQPExchange($ch);
            $xchg->setName($details['exchange']);
            $xchg->setType('topic');
            $xchg->setFlags($this->getFlags($opts));
            $xchg->declareExchange();

            $exchanges[$details['exchange']] = $this->xchg = $xchg;
        }

        if (!empty($queues[$details['binding']])) {
            $this->q = $queues[$details['binding']];
        } else {
            $q = new AMQPQueue($ch);
            $q->setName($details['binding']);
            $q->setFlags($this->getFlags($opts));

            if (!empty($expire)) {
                $this->q->setArgument("x-expires", $expire);
            }

            $this->q = $queues[$details['binding']] = $q;

            $this->q->declareQueue();

            try {
                $this->q->bind($details['exchange'], $details['routing_key']);
            } catch (AMQPQueueException $e) {
                return false;
            }
        }
    }

    /**
     * Return flags for declare queue and exchange
     *
     * @param  array $opts options for declare queue and exchange
     * @return int
     */
    public function getFlags(array $opts)
    {
        $flags = 0;

        if (!empty($opts)) {
            if (isset($opts['AUTODELETE']) && $opts['AUTODELETE']) {
                $flags |= AMQP_AUTODELETE;
            }

            if (isset($opts['DURABLE']) && $opts['DURABLE']) {
                $flags |= AMQP_DURABLE;
            }
        } else {
            $flags = AMQP_AUTODELETE | AMQP_DURABLE;
        }

        return $flags;
    }

    /**
     * Pool of queues. Return queue if it exists
     *
     * @param  array  $queues
     * @param  string $binding name of queue
     * @return bool
     */
    private function getQueue(array $queues, $binding)
    {
        return isset($queues[$binding]) ? $queues[$binding] : false;
    }
}
